﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace CounterToolPrism.View
{
    public class MinMaxConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var val = value as int?;
                return (val == null) ? "" : val.ToString();
            }
            catch (Exception e) when (e is InvalidCastException)
            {
                return "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var val = value as string;
                return (val == "") ? (int?)null : new int?(Int32.Parse(val));
            }
            catch (Exception e) when (e is FormatException || e is InvalidCastException)
            {
                return null;
            }
        }
    }
}
