﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CounterToolPrism.Model
{
    public class CounterHolder : BindableBase
    {
        #region Members definition
        #region Static members definition
        private const string DEFAULT_COUNTER_TITLE = "Counter";
        #endregion Static members definition

        #region Fields definition
        //private readonly List<Counter> _counters;
        //private ObservableCollection<Counter> _countersObservable;
        #endregion Fields definition

        #region Properties definition
        // TODO: This could probably all be reworked to be quite a bit cleaner.
        // Maybe the ObservableCollection doesn't need to be backed by a List since the elements only need to have unique IDs.
        //if (this._countersObservable == null)
        //{ this.SetProperty(ref this._countersObservable, new ObservableCollection<Counter>(this._counters)); }
        ////{ this._countersObservable = new ObservableCollection<Counter>(this._counters); }
        //return this._countersObservable;
        public ObservableCollection<Counter> Counters { get; private set; }
        #endregion Properties definition
        #endregion Members definition

        #region Constructors definition
        public CounterHolder()
        {
            this.Counters = new ObservableCollection<Counter>();
        }
        #endregion Constructors definition

        #region Methods definition
        public void Add()
        {
            this.Counters.Add(new Counter(this.Counters.Count + 1, DEFAULT_COUNTER_TITLE));
        }

        public void Remove(int id)
        {
            //this.Counters.RemoveAll(counter => counter.Id == id);
            throw new NotImplementedException();
        }

        public void Remove(Counter counter)
        {
            this.Counters.Remove(counter);
        }

        public Counter GetByID(int id)
        {
            return this.Counters.Where(counter => counter.Id == id).First();
        }

        public void Increment(int id)
        {
            this.GetByID(id).Increment();
        }

        public void Decrement(int id)
        {
            this.GetByID(id).Decrement();
        }
        #endregion Methods definition
    }
}
