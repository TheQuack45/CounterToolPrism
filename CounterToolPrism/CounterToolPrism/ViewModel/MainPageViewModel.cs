﻿using CounterToolPrism.Model;
using DLToolkit.Forms.Controls;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CounterToolPrism.ViewModel
{
    public class MainPageViewModel : ViewModelBase
    {
        #region Members definition
        #region Fields definition
        private readonly CounterHolder _counterHolder;
        #endregion Fields definition

        #region Properties definition
        public ObservableCollection<Counter> Counters
        {
            //get { return new FlowObservableCollection<Counter>(this._counterHolder.Counters); }
            get { return this._counterHolder.Counters; }
        }
        #endregion Properties definition

        #region Commands definition
        private ICommand _addCounterCommand;
        public ICommand AddCounterCommand
        {
            get
            {
                if (this._addCounterCommand == null)
                    { this._addCounterCommand = new Command(() => this.AddCounter()); }
                return this._addCounterCommand;
            }
        }

        private ICommand _incrementCommand;
        public ICommand IncrementCommand
        {
            get
            {
                if (this._incrementCommand == null)
                    { this._incrementCommand = new Command((id) => this._counterHolder.Increment((int)id)); }
                return this._incrementCommand;
            }
        }

        private ICommand _decrementCommand;
        public ICommand DecrementCommand
        {
            get
            {
                if (this._decrementCommand == null)
                    { this._decrementCommand = new Command((id) => this._counterHolder.Decrement((int)id)); }
                return this._decrementCommand;
            }
        }
        #endregion Commands definition
        #endregion Members definition

        public MainPageViewModel(INavigationService navigationService) 
            : base (navigationService)
        {
            Title = "Main Page";

            this._counterHolder = new CounterHolder();
        }

        public void AddCounter()
        {
            //this.Counters.Add(new Counter(1, "garb"));
            this._counterHolder.Add();
        }
    }
}
